package iaf.ofek.hadracha.base_course.web_server.EjectedPilotRescue;

import iaf.ofek.hadracha.base_course.web_server.AirSituation.Airplane;
import iaf.ofek.hadracha.base_course.web_server.Data.Coordinates;
import iaf.ofek.hadracha.base_course.web_server.Data.Entity;

import java.util.ArrayList;
import java.util.List;

public class EjectedPilotInfo implements Entity<EjectedPilotInfo> {
    private int id;
    private List<AllocatedAirplane> allocatedAirplanes = new ArrayList<>();
    private Coordinates coordinates;
    private String pilotName;

    /**
     * The rescue manager's client id, or null if non.
     */
    private String rescuedBy;

    private  AllocatedAirplane allocatedAirplane;


    @Override
    public int getId() {
        return this.id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    @Override
    public EjectedPilotInfo clone() {
        try {
            return (EjectedPilotInfo) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new AssertionError("Not possible - Entity implements Cloneable");
        }
    }

    public void allocateAirplane(Airplane airplane, String controllerClientId){
        AllocatedAirplane allocatedAirplane = new AllocatedAirplane(airplane);
        this.allocatedAirplanes.add(allocatedAirplane);
        airplane.flyTo(this.coordinates, controllerClientId);
        airplane.onArrivedAtDestination(this::airplaneArrived);
    }

    private void airplaneArrived(Airplane airplane){

        this.allocatedAirplanes.stream()
                .filter(allocatedAirplane -> allocatedAirplane.getAirplane().getId() == airplane.getId())
                .forEach(allocatedAirplane -> allocatedAirplane.setArrivedAtDestination(true));

        if (this.allocatedAirplanes.stream().allMatch(allocatedAirplane -> allocatedAirplane.getArrivedAtDestination())){
            this.allocatedAirplanes.forEach(allocatedAirplane -> allocatedAirplane.getAirplane().unAllocate());
        }
    }

    public String getRescuedBy() {
        return this.rescuedBy;
    }

    public void setRescuedBy(String rescuedBy) {
        this.rescuedBy = rescuedBy;
    }

    public String getPilotName() {
        return this.pilotName;
    }

    public void setPilotName(String pilotName) {
        this.pilotName = pilotName;
    }

    public Coordinates getCoordinates() {
        return this.coordinates;
    }

    public void setCoordinates(Coordinates coordinates) {
        this.coordinates = coordinates;
    }
}
