package iaf.ofek.hadracha.base_course.web_server.Data;

public class Coordinates {
    private double lat;
    private double lon;

    public Coordinates() {
    }

    @Override
    public String toString() {
        return String.format("N%.6fE%.6f",this.lat, this.lon);
    }

    public Coordinates(double lat, double lon) {
        this.lat = lat;
        this.lon = lon;
    }

    public double getLat() {
        return this.lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return this.lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }
}

