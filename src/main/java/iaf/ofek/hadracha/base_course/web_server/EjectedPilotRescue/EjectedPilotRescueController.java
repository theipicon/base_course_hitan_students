package iaf.ofek.hadracha.base_course.web_server.EjectedPilotRescue;

import iaf.ofek.hadracha.base_course.web_server.Data.InMemoryMapDataBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/ejectedPilotRescue")
public class EjectedPilotRescueController {
    @Autowired
    private InMemoryMapDataBase inMemoryMapDataBase;

    @Autowired
    private AirplanesAllocationManager airplanesAllocationManager;

    @GetMapping("/infos")
    public List<EjectedPilotInfo> getInfos() {
        return this.inMemoryMapDataBase.getAllOfType(EjectedPilotInfo.class);
    }

    @GetMapping("/takeResponsibility")
    public void takeResponsibility(@RequestParam int ejectionId, @CookieValue ( value = "client-id") String clientId ){
        EjectedPilotInfo ejectedPilot = this.inMemoryMapDataBase.getByID(ejectionId, EjectedPilotInfo.class);
        
        if (ejectedPilot.getRescuedBy() == null){
            this.airplanesAllocationManager.allocateAirplanesForEjection(ejectedPilot, clientId);
            ejectedPilot.setRescuedBy(clientId);
            this.inMemoryMapDataBase.update(ejectedPilot);
        }
    }
 }
