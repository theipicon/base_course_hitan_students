package iaf.ofek.hadracha.base_course.web_server.EjectedPilotRescue;

import iaf.ofek.hadracha.base_course.web_server.AirSituation.Airplane;

public class AllocatedAirplane {
    private Airplane airplane;
    private boolean arrivedAtDestination;

    AllocatedAirplane(Airplane airplane) {
        this.airplane = airplane;
    }

    public Airplane getAirplane(){
        return this.airplane;
    }

    public boolean getArrivedAtDestination(){
        return this.arrivedAtDestination;
    }

    public void setArrivedAtDestination(Boolean arrivedAtDestination){
        this.arrivedAtDestination = arrivedAtDestination;
    }
}
